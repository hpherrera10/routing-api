# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

puts 'Creating User...'
User.create(email: "admin@email.com", national_nid: "11.111.111-k", subject: "API-KEY-INTEGRATION")

puts 'Creating Organization...'
Organization.create(name: "Routing")
Organization.create(name: "Pricing")

puts 'Creating Vehicle...'
Vehicle.create(plate: "VEHI-01", organization_id: 1)
Vehicle.create(plate: "VEHI-02", organization_id: 1)
Vehicle.create(plate: "VEHI-03", organization_id: 1)
Vehicle.create(plate: "VEHI-04", organization_id: 1)
Vehicle.create(plate: "VEHI-05", organization_id: 1)
Vehicle.create(plate: "VEHI-06", organization_id: 2)
Vehicle.create(plate: "VEHI-07", organization_id: 2)
Vehicle.create(plate: "VEHI-08", organization_id: 2)

puts 'Creating Driver...'
Driver.create(name: "Carlos", last_name: "Apellido", vehicle_id: 2, organization_id: 1)
Driver.create(name: "Diego", last_name: "Apellido", vehicle_id: 1, organization_id: 1)
Driver.create(name: "Adan", last_name: "Apellido", vehicle_id: 3, organization_id: 1)
Driver.create(name: "Andrea", last_name: "Apellido", vehicle_id: 7, organization_id: 2)
Driver.create(name: "Mauro", last_name: "Apellido", vehicle_id: 8, organization_id: 2)

puts 'Creating Route...'
Route.create(starts_at: "07/18/2022 09:00", ends_at: "07/18/2022 11:30", travel_time: "2h 30m", total_stops: 3, action: "Llegada", driver_id: nil, organization_id: 1)
Route.create(starts_at: "07/18/2022 09:00", ends_at: "07/18/2022 10:10", travel_time: "1h 30m", total_stops: 2, action: "Llegada", driver_id: nil, organization_id: 1)
Route.create(starts_at: "07/18/2022 11:00", ends_at: "07/18/2022 13:30", travel_time: "2h 30m", total_stops: 3, action: "Llegada", driver_id: 2, organization_id: 1)
