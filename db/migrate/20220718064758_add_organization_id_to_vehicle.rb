class AddOrganizationIdToVehicle < ActiveRecord::Migration[5.1]
  def change
    add_reference :vehicles, :organization, foreign_key: true
  end
end
