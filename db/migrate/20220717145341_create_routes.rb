class CreateRoutes < ActiveRecord::Migration[5.1]
  def change
    create_table :routes do |t|
      t.string :starts_at
      t.string :ends_at
      t.string :travel_time
      t.integer :total_stops
      t.string :action
      t.references :organization, foreign_key: true
      t.references :driver, foreign_key: true

      t.timestamps
    end
  end
end
