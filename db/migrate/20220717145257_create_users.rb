class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :subject
      t.string :email
      t.string :national_nid
      
      t.timestamps
    end
  end
end
