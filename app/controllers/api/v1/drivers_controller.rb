class Api::V1::DriversController < ApplicationController
  # GET /api/v1/drivers
  def index
    @drivers = Driver.where(organization_id: params[:organization_id])

    render json: @drivers
  end
end
