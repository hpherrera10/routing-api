class Api::V1::VehiclesController < ApplicationController
  # GET /api/v1/vehicles
  def index
    @vehicles = Vehicle.where(organization_id: params[:organization_id])

    render json: @vehicles
  end
end
