class Api::V1::OrganizationsController < ApplicationController
  # GET /api/v1/organizations
  def index
    @organizations = Organization.all

    render json: @organizations
  end

  # GET /api/v1/organizations/:id
  def show
    @organization = Organization.find(params[:id])

    render json: @organization
  end
end