class Api::V1::RoutesController < ApplicationController
  # GET /api/v1/routes
  def index
    @routes = Route.where(organization_id: params[:organization_id])

    render json: @routes
  end

  def update
    @route = Route.find(params[:id])
    @route.update_attributes(route_param)
    render json: @route
  end

  private

  def route_param
    params.require(:route).permit(:starts_at, :ends_at, :travel_time, :total_stops, :action, :driver_id)
  end
end
