Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do 
      resources :organizations, only: %i[index show]

      resources :drivers, only: :index

      resources :vehicles, only: :index

      resources :routes, only: %i[index create update]
    end
  end
end
