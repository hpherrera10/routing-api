# routing-api



## Getting started

## Getting started

#### Pre requisites

- Ruby 2.6.5
- Rails 5.1
- PostgreSQL 12

```shell

brew install postgresql@12
brew services start postgresql@12

gem install bundler
bundle install
```


#### Database

```shell
rails db:create
rails db:migrate
rails db:seed
```

#### Rails server

```shell
rails s -p 5000
```

